require "auth_spoof/version.rb"
require "auth_spoof/configuration.rb"
require "auth_spoof/login_strategy.rb"
require "auth_spoof/login_strategy/base.rb"
require "auth_spoof/login_strategy/confluence.rb"

module AuthSpoof
	def self.config
		AuthSpoof::Configuration.instance
	end
end
