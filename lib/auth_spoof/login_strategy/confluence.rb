require "uri"
require "logger"
require "ostruct"

module AuthSpoof::LoginStrategy
  class Confluence < Base
    def initialize(logger: Logger.new(STDOUT))
      super
    end

    def login
      fail RuntimeError if credentials.login.nil? || credentials.password.nil?

      @cookies = {}
      page.reset!
      page.driver.clear_network_traffic

      @logger.info('Visiting login page')
      page.visit(login_path)

      @logger.info('Filling user info')
      page.within('#login-container') do

        page.fill_in('Username', with: credentials.login)
        page.fill_in('Password', with: credentials.password)
        page.check('Remember me')

        @logger.info('Logging in')
        page.click_button('Log in')
      end

      @cookies = page.driver.cookies
      @logger.info('Success')

      self

    rescue => exception
      @logger.error(exception)
      raise exception
    end

    def cookies
      cookies_map = @cookies.map do |name, p_cookie|
        cookie_attributes = {
          http_only: p_cookie.httponly?,
          path:      p_cookie.path,
          secure:    p_cookie.secure?,
          value:     p_cookie.value,
          expires:   p_cookie.expires,
        }

        [name, cookie_attributes]
      end

      cookies_map.to_h
    end

    def headers
      response = login_request.response_parts[0]

      req_header = login_request.headers.map { |h| [h['name'], h['value']]}.to_h
      resp_headers = response.headers.map { |h| [h['name'], h['value']]}.to_h

      {
        'X-ASEN' => resp_headers['X-ASEN'],
        'X-Confluence-Request-Time' => resp_headers['X-Confluence-Request-Time'],
        'Host' => URI(login_path).host,
        'Referer' => login_path
      }
    end

    private

    def login_request
      page.driver.network_traffic[2]
    end
  end
end
