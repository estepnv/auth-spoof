require 'capybara/dsl'
require 'capybara/node/actions'

module AuthSpoof::LoginStrategy
  class Base

    attr_accessor :login_path
    attr_reader :credentials, :session

    alias_method :page, :session

    def initialize(logger:)
      @credentials = OpenStruct.new
      @cookies = {}
      @login_path = nil
      @logger     = logger
      @session    = Capybara::Session.new(AuthSpoof.config.capybara_driver)

      page.driver.headers = {
        "User-Agent" => "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0"
      }
    end

    def dispose
      page.driver.quit
    end

    def user_agent=(value)
      page.driver.headers = {'User-Agent' => value}
    end

    def cookies
      fail NotImplementedError
    end

    def headers
      fail NotImplementedError
    end

    def assign_headers(hash)
      headers.each do |name, attributes|
        hash[name] = attributes
      end
    end

    def assign_cookies(hash)
      cookies.each do |name, attributes|
        hash[name] = attributes
      end
    end

    def login
      fail NotImplementedError
    end

    def screenshot
      page.save_screenshot(Rails.root.join("tmp/confluence_#{Kernel.srand(10)}.png").to_s, full: true)
    end
  end
end
