require "singleton"

module AuthSpoof
	class Configuration
		include Singleton

		attr_accessor :capybara_driver

		def initialize
			self.capybara_driver = :poltergeist
		end
	end
end